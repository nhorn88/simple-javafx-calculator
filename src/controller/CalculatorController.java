/*
 * File: CalculatorController.java
 * Author: NHorn
 * Date Modified: 5/14/18
 * 
 * Description: Handles logic for Calculator functions.
 * 				Initialized by CalculatorFXML.fxml.
 * 				Supports simple two operand operations.
 */

package controller;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;

public class CalculatorController {
	
	//FXML Injected Variables
	@FXML private TextArea outputArea;
	@FXML private GridPane buttonGrid;
	@FXML private Button clearButton;
	@FXML private Button addButton;
	@FXML private Button subButton;
	@FXML private Button multButton;
	@FXML private Button divButton;
	
	//Other Variables
	private double operandOne = 0;
	private double operandTwo = 0;
	private double resultTotal = 0;
	private String operandOneString = "0";
	private String operandTwoString = "0";
	private char operator = '!';
	private boolean operandOneSet = false;
	private boolean operandTwoSet = false;
	private boolean operatorSet = false;
	private boolean decimalUsed = false;
	private boolean resultDisplayed = false;
	
	/*
	 * Private logic methods
	 * 
	 */
	
	private void cleanUp() {
		operandOne = 0;
		operandTwo = 0;
		operator = '!';
		operandOneString = "0";
		operandTwoString = "0";
		operandOneSet = false;
		operandTwoSet = false;
		operatorSet = false;
		decimalUsed = false;
	}
	
	private void parseOperand(String opString, int opNum) {
		double result = 0;
		if(opString.contentEquals("")) {
			//skip to end
		}
		else { //parse
			result = Double.parseDouble(opString);
		}
		
		if(opNum == 1) {
			operandOne = result;
		}
		else if(opNum == 2) {
			operandTwo = result;
		}
		decimalUsed = false;
	}
	
	private void numberInput(String num) {
		if(resultDisplayed == true && operatorSet == false) {
			outputArea.setText("");
			resultDisplayed = false;
		}
		
		outputArea.appendText(num);
		
		if(operandOneSet == false) {
			operandOneString += num;
		}
		else {
			disableOperatorButtons();
			operandTwoString += num;
		}
	}
	
	private void operatorInput(char op) {
		if(resultDisplayed == true) {
			operandOne = resultTotal;
		}
		
		if(operandOneSet == false && resultDisplayed == false) {
			parseOperand(operandOneString, 1);
		}
		
		operandOneSet = true;

		if(operandTwoSet == false) {
			operator = op;
			
			if(operatorSet == false) {
				outputArea.appendText(" "+ operator +" ");
				operatorSet = true;
			}
			else {
				outputArea.setText("" + operandOne + " "+ operator +" ");
			}
		}
		resultDisplayed = false;
	}
	
	private void setDivByZeroError() {
		outputArea.setText("DIV BY ZERO ERROR");
		ObservableList<Node> buttons = buttonGrid.getChildren();
		for(Node button : buttons) {
			button.setDisable(true);
		}
		clearButton.setDisable(false);
		disableOperatorButtons();
	}
	
	private void clearDivByZeroError() {
		ObservableList<Node> buttons = buttonGrid.getChildren();
		for(Node button : buttons) {
			button.setDisable(false);
		}
		enableOperatorButtons();
	}
	
	private void disableOperatorButtons() {
		addButton.setDisable(true);
		subButton.setDisable(true);
		multButton.setDisable(true);
		divButton.setDisable(true);
	}
	
	private void enableOperatorButtons() {
		addButton.setDisable(false);
		subButton.setDisable(false);
		multButton.setDisable(false);
		divButton.setDisable(false);
	}
	
	/*
	 * Event Handlers for FXML
	 * 
	 */
	
	public void closeProgram(ActionEvent event) {
		System.exit(0);
	}
	
	public void clearButton(ActionEvent event) {
		clearDivByZeroError();
		cleanUp();
		resultTotal = 0;
		outputArea.setText("");
		resultDisplayed = false;
	}
	
	public void equalsButton(ActionEvent event) {
		if(operandOneSet == false) {
			if(resultTotal != 0.0 && resultDisplayed == true) {
				operandOne = resultTotal;
			}
			else{
				parseOperand(operandOneString, 1);
				operandOneSet = true;
			}
		}
		
		operandTwoSet = true;
		parseOperand(operandTwoString, 2);
		boolean divError = false;
		
		switch(operator) {
			case '+':
				resultTotal = operandOne + operandTwo;
				break;
				
			case '-':
				resultTotal = operandOne - operandTwo;
				break;
				
			case '*':
				resultTotal = operandOne * operandTwo;
				break;
				
			case '/':
				if(operandTwo == 0) {
					divError = true;
				}
				else {
					resultTotal = operandOne / operandTwo;
				}
				break;
				
			default:
				resultTotal = operandOne;
		}//end switch
		
		if(divError == true) {
			setDivByZeroError();
		}
		else {
			outputArea.setText("" + resultTotal);
			enableOperatorButtons();
		}
		resultDisplayed = true;
		cleanUp();
	}//end equalsButton
 	
	public void decimalButton(ActionEvent event) {
		if(resultDisplayed == true && operatorSet == false) {
			outputArea.setText("");
			resultDisplayed = false;
		}
		
		String num = ".";
		
		if(decimalUsed == false) {
			outputArea.appendText(num);
			
			if(operandOneSet == false) {
				operandOneString += num;
			}
			else if(operandTwoSet == false){
				operandTwoString += num;
			}
		}
		decimalUsed = true;
	}
	
	public void addButton(ActionEvent event) {
		operatorInput('+');
	}
	
	public void subButton(ActionEvent event) {
		operatorInput('-');
	}
	
	public void multButton(ActionEvent event) {
		operatorInput('*');
	}
	
	public void divButton(ActionEvent event) {
		operatorInput('/');
	}
	
	public void zeroButton(ActionEvent event) {
		numberInput("0");
	}
	
	public void oneButton(ActionEvent event) {
		numberInput("1");
	}
	
	public void twoButton(ActionEvent event) {
		numberInput("2");
	}
	
	public void threeButton(ActionEvent event) {
		numberInput("3");
	}
	
	public void fourButton(ActionEvent event) {
		numberInput("4");
	}
	
	public void fiveButton(ActionEvent event) {
		numberInput("5");
	}
	
	public void sixButton(ActionEvent event) {
		numberInput("6");
	}
	
	public void sevenButton(ActionEvent event) {
		numberInput("7");
	}
	
	public void eightButton(ActionEvent event) {
		numberInput("8");
	}
	
	public void nineButton(ActionEvent event) {
		numberInput("9");
	}
	
}//end class
