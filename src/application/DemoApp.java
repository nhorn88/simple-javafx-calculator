/*
 * File: DemoApp.java
 * Author: NHorn
 * Date Modified: 5/14/18
 * 
 * Description: Driver class for FXML/JavaFX Calculator Demo
 * 				FXMLLoader initializes CalculatorController when it loads
 * 				the CalculatorFXML.fxml file.
 */

package application;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class DemoApp extends Application {
    
    public static void main(String[] args) {
        Application.launch(DemoApp.class, args);
    }
    
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("../fxml/CalculatorFXML.fxml"));
        
        stage.setTitle("CS366 FXML Demo!");
        stage.setScene(new Scene(root)); //add two more fields to set default height/width
        stage.setResizable(false);
        stage.show();
    }
}